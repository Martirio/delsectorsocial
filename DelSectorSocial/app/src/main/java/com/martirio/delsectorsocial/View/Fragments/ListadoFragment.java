package com.martirio.delsectorsocial.View.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.martirio.delsectorsocial.Model.Page;
import com.martirio.delsectorsocial.Model.PaginaWiki;
import com.martirio.delsectorsocial.R;
import com.martirio.delsectorsocial.Utils.ResultListener;
import com.martirio.delsectorsocial.View.Adapter.AdapterPalabrasListado;
import com.martirio.delsectorsocial.View.Controller.ControllerDatos;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListadoFragment extends Fragment {


    private TextView tituloCategoria;

    public ListadoFragment() {
        // Required empty public constructor
    }

    public static final String ORIGEN="ORIGEN";
    public static final String PALABRABUSCADA="PALABRABUSCADA";
    public static final String NUMEROPAGINABUSCADA="NUMEROPAGINABUSCADA";

    private ControllerDatos controllerDatosPalabraCategoria;

    private String elOrigen;
    private String laPalabra;
    private String elNumeroPagina;
    private Listable listable;
    private TextView textoSugerir;

    private RecyclerView recycler;
    private AdapterPalabrasListado adapterPalabrasListado;

    public interface Listable {
       void abrirPalabra(String unString);
       void abrirSugerencias();
    }

    private FloatingActionButton fabSugerir;

    private SharedPreferences config;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_listado, container, false);

        Bundle bundle=getArguments();
        if (bundle!=null){
            elOrigen=bundle.getString(ORIGEN);
            laPalabra=bundle.getString(PALABRABUSCADA);
        }

        recycler=view.findViewById(R.id.recyclerPalabrasCategoria);
        tituloCategoria = view.findViewById(R.id.tituloCategoria);
        fabSugerir = view.findViewById(R.id.fabSugerir);
        textoSugerir= view.findViewById(R.id.textoSugerir);

        adapterPalabrasListado= new AdapterPalabrasListado();
        adapterPalabrasListado.setContext(getContext());


        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recycler.setLayoutManager(linearLayoutManager);
        adapterPalabrasListado.setListaPalabrasOriginales(new ArrayList<String>());
        adapterPalabrasListado.notifyDataSetChanged();
        recycler.setAdapter(adapterPalabrasListado);

        if (elOrigen.equals("categoria")) {
            traerPalabrasCategoria(laPalabra);
            fabSugerir.setVisibility(View.GONE);
            textoSugerir.setVisibility(View.GONE);

        } else if (elOrigen.equals("buscada")){
            traerPalabraConString(laPalabra);

            fabSugerir.setVisibility(View.VISIBLE);
            textoSugerir.setVisibility(View.VISIBLE);
            fabSugerir.setColorNormal(getContext().getResources().getColor(R.color.violeta2));
            fabSugerir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listable.abrirSugerencias();
                }
            });

        }

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer posicion=recycler.getChildAdapterPosition(view);
                List<String> unaLista=adapterPalabrasListado.getListaPalabrasOriginales();
                String unaCat=unaLista.get(posicion);
                listable.abrirPalabra(unaCat);
            }
        };


        adapterPalabrasListado.setListener(listener);


        return view;
    }

    private void traerPalabraConString(String laPalabra) {
        config = ListadoFragment.this.getActivity().getSharedPreferences("prefs",0);
        final String categoriaElegida =config.getString("categoriaElegida","Argentina");
        controllerDatosPalabraCategoria= new ControllerDatos(getContext());

        tituloCategoria.setText(laPalabra);
        controllerDatosPalabraCategoria.traerTitulosConPalabra(new ResultListener<PaginaWiki>() {
            @Override
            public void finish(PaginaWiki resultado) {
                List<String> listaPalabras=new ArrayList<>();
                if (resultado!=null && resultado.getData()!=null && resultado.getData().getPagesCat()!=null) {
                    for (Page unaPage:resultado.getData().getPagesCat()
                            ) {

                        listaPalabras.add(unaPage.getTitle());
                    }
                }
                actualizarRecycler(listaPalabras);
            }
        },laPalabra,categoriaElegida);
    }

    private void traerPalabrasCategoria(String laPalabra) {
        controllerDatosPalabraCategoria= new ControllerDatos(getContext());
        String laPalabraModificada=laPalabra.replace("_"," ");
        tituloCategoria.setText(laPalabraModificada);
        controllerDatosPalabraCategoria.traerPalabrasConCategorias(new ResultListener<PaginaWiki>() {
            @Override
            public void finish(PaginaWiki resultado) {
                List<String> listaPalabras=new ArrayList<>();
                if (resultado!=null && resultado.getData()!=null && resultado.getData().getPagesCat()!=null) {
                    for (Page unaPage:resultado.getData().getPagesCat()
                            ) {

                        listaPalabras.add(unaPage.getTitle());
                    }
                }
                actualizarRecycler(listaPalabras);
            }
        },laPalabra);
    }

    private void actualizarRecycler(List<String> listaPalabras) {
        adapterPalabrasListado.setListaPalabrasOriginales(listaPalabras);
        adapterPalabrasListado.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listable=(Listable)context;
    }
}
