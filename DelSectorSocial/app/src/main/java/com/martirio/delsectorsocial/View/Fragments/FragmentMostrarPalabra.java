package com.martirio.delsectorsocial.View.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.martirio.delsectorsocial.Model.Page;
import com.martirio.delsectorsocial.Model.PaginaWiki;
import com.martirio.delsectorsocial.R;
import com.martirio.delsectorsocial.Utils.HTTPConnectionManager;
import com.martirio.delsectorsocial.Utils.ResultListener;
import com.martirio.delsectorsocial.View.Adapter.AdapterPalabrasFuentes;
import com.martirio.delsectorsocial.View.Adapter.AdapterPalabrasListado;
import com.martirio.delsectorsocial.View.Controller.ControllerDatos;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMostrarPalabra extends Fragment {

    public static final String ORIGEN="ORIGEN";
    public static final String PALABRABUSCADA="PLABRABUSCADA";
    public static final String NUMEROPAGINABUSCADA="NUMEROPAGINABUSCADA";

    private String elOrigen;
    private String laPalabra;
    private String elNumeroPagina;

    private ControllerDatos controllerDatos;

    private TextView tituloPalabra;
    private TextView contenidoPalabra;
    private TextView tituloPalabraRelacionadas;
    private  TextView tituloAlternativos;
    private  TextView tituloFuentes;


    private RecyclerView recyclerTerminosRelacionados;
    private RecyclerView recyclerTerminosAlternativos;
    private RecyclerView recyclerFuentes;

    private AdapterPalabrasListado adapterRelacionados;
    private AdapterPalabrasListado adapterAlternativos;
    private AdapterPalabrasFuentes adapterFuentes;

    private Relacionable relacionable;

    private com.github.clans.fab.FloatingActionButton agrandar;
    private com.github.clans.fab.FloatingActionButton achicar;
    private ProgressBar progressBar;

    private Map<String, String>listaFuentes;

    private SharedPreferences config;


    public FragmentMostrarPalabra() {
        // Required empty public constructor
    }

    public interface Relacionable{
        void abrirTerminoRelacionado(String terminoRelacionado);
        void abrirFuente(String unString);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_mostrar_palabra, container, false);

        if (getActivity()!=null) {
            config = getActivity().getSharedPreferences("prefs",0);
        }

        listaFuentes=new HashMap<>();
        tituloPalabra=view.findViewById(R.id.tituloMostrarPalabra);
        contenidoPalabra=view.findViewById(R.id.descripcionPalabra);
        tituloPalabraRelacionadas=view.findViewById(R.id.tituloRecyclerRelacionados);
        tituloAlternativos=view.findViewById(R.id.tituloRecyclerAlternativos);
        tituloFuentes=view.findViewById(R.id.tituloRecyclerFuentes);

        recyclerTerminosRelacionados = view.findViewById(R.id.recyclerTerminosRelacionados);
        recyclerTerminosAlternativos = view.findViewById(R.id.recyclerTerminosAlternativos);
        recyclerFuentes = view.findViewById(R.id.recyclerFuentes);

        agrandar=view.findViewById(R.id.agrandarLetra);
        achicar=view.findViewById(R.id.achicarLetra);

        progressBar=view.findViewById(R.id.progressBarVerDetalle);

        adapterRelacionados =new AdapterPalabrasListado();
        adapterAlternativos=new AdapterPalabrasListado();
        adapterFuentes=new AdapterPalabrasFuentes();

        adapterRelacionados.setContext(getContext());
        adapterAlternativos.setContext(getContext());
        adapterFuentes.setContext(getContext());

        LinearLayoutManager linearRelacionados =new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        LinearLayoutManager linearAlternativos =new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        LinearLayoutManager linearFuentes =new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerTerminosRelacionados.setLayoutManager(linearRelacionados);
        recyclerTerminosAlternativos.setLayoutManager(linearAlternativos);
        recyclerFuentes.setLayoutManager(linearFuentes);

        adapterRelacionados.setListaPalabrasOriginales(new ArrayList<String>());
        adapterAlternativos.setListaPalabrasOriginales(new ArrayList<String>());
        adapterFuentes.setListaPalabrasOriginales(new ArrayList<String>());

        recyclerTerminosRelacionados.setAdapter(adapterRelacionados);
        recyclerTerminosAlternativos.setAdapter(adapterAlternativos);
        recyclerFuentes.setAdapter(adapterFuentes);


        View.OnClickListener listenerRelacionados = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (HTTPConnectionManager.isNetworkingOnline(view.getContext())) {
                    Integer posicion=recyclerTerminosRelacionados.getChildAdapterPosition(view);
                    List<String> unaLista= adapterRelacionados.getListaPalabrasOriginales();
                    String unString=unaLista.get(posicion);
                    relacionable.abrirTerminoRelacionado(unString);
                }
                else{
                    Snackbar elSnack = Snackbar.make(getView(),getContext().getString(R.string.sinConexion),Snackbar.LENGTH_LONG);
                    elSnack.show();
                }
            }
        };

        View.OnClickListener listenerAlternativos = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (HTTPConnectionManager.isNetworkingOnline(view.getContext())) {
                    Integer posicion=recyclerTerminosAlternativos.getChildAdapterPosition(view);
                    List<String> unaLista= adapterAlternativos.getListaPalabrasOriginales();
                    String unString=unaLista.get(posicion);
                    relacionable.abrirTerminoRelacionado(unString);
                } else {
                    Snackbar elSnack = Snackbar.make(getView(),getContext().getString(R.string.sinConexion),Snackbar.LENGTH_LONG);
                    elSnack.show();
                }
            }
        };

        View.OnClickListener listenerFuentes = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (HTTPConnectionManager.isNetworkingOnline(view.getContext())) {
                    Integer posicion=recyclerFuentes.getChildAdapterPosition(view);
                    List<String> unaLista= adapterFuentes.getListaPalabrasOriginales();
                    String unString=unaLista.get(posicion);
                    String link = listaFuentes.get(unString);
                    relacionable.abrirFuente(link);
                }
                else{
                    Snackbar elSnack = Snackbar.make(getView(),getContext().getString(R.string.sinConexion),Snackbar.LENGTH_LONG);
                    elSnack.show();
                }
            }
        };


        adapterRelacionados.setListener(listenerRelacionados);
        adapterAlternativos.setListener(listenerAlternativos);
        adapterFuentes.setListener(listenerFuentes);

        Bundle bundle=getArguments();
        if (bundle!=null){
            elOrigen=bundle.getString(ORIGEN);
            laPalabra=bundle.getString(PALABRABUSCADA);
            elNumeroPagina=bundle.getString(NUMEROPAGINABUSCADA);
        }
        controllerDatos= new ControllerDatos(getContext());
        
        if (elOrigen.equals("busqueda")){
            cargarDatosConPalabra(laPalabra);
        }
        else{
            cargarDatosConNumero();
        }

        agrandar.setImageResource(R.drawable.ic_zoom_in_black_24dp);
        agrandar.setColorNormal(getContext().getResources().getColor(R.color.violeta2));
        agrandar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contenidoPalabra.setTextSize(TypedValue.COMPLEX_UNIT_PX, contenidoPalabra.getTextSize() + 2);

                SharedPreferences.Editor editor = config.edit();
                editor.putFloat("tamanioTexto",contenidoPalabra.getTextSize());
                editor.commit();

            }
        });

        achicar.setImageResource(R.drawable.ic_zoom_out_black_24dp);
        achicar.setColorNormal(getContext().getResources().getColor(R.color.violeta2));
        achicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contenidoPalabra.setTextSize(TypedValue.COMPLEX_UNIT_PX, contenidoPalabra.getTextSize() -2);

                SharedPreferences.Editor editor = config.edit();
                editor.putFloat("tamanioTexto",contenidoPalabra.getTextSize());
                editor.commit();
            }
        });

        final ScrollView main = (ScrollView) view.findViewById(R.id.scrollview);
        main.post(new Runnable() {
            public void run() {
                main.scrollTo(0,0);
            }
        });



        return view;
    }

    private void cargarDatosConNumero() {
    }

    private void cargarDatosConPalabra(final String laPalabra) {
        progressBar.setVisibility(View.VISIBLE);
        controllerDatos.traerDatosConTituloExacto(new ResultListener<PaginaWiki>() {
            @Override
            public void finish(PaginaWiki paginaWiki) {
                if (paginaWiki!=null && paginaWiki.getData()!=null && paginaWiki.getData().getPages()!=null && paginaWiki.getData().getPages().entrySet().iterator().next().getValue().getRevisions()!=null) {

                    Map.Entry unaEntry = paginaWiki.getData().getPages().entrySet().iterator().next();
                    String pagina = unaEntry.getKey().toString();
                    Page unaPagina = (Page) unaEntry.getValue();


                    String textoTituloParamostrar = unaPagina.getTitle();
                    String textoParaMostrar = unaPagina.getRevisions().get(0).getContenido();
                    String[] parts = textoParaMostrar.split(Pattern.quote("{{"));

                    textoParaMostrar = parts[1].substring(18);
                    textoParaMostrar = textoParaMostrar.replace("}}", "");
                    textoParaMostrar = textoParaMostrar.replace("[[", "");
                    textoParaMostrar = textoParaMostrar.replace("[[", "");
                    textoParaMostrar = textoParaMostrar.replace("[", "");
                    textoParaMostrar = textoParaMostrar.replace("]", "");


                    Typeface robotoR = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
                    Typeface robotoL = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf");

                    tituloPalabra.setText(textoTituloParamostrar);
                    tituloAlternativos.setTypeface(robotoR);

                    contenidoPalabra.setText(textoParaMostrar);
                    final float tamanioLetra = config.getFloat("tamanioTexto",1234f);
                    if (tamanioLetra!=1234f) {
                        contenidoPalabra.setTextSize(TypedValue.COMPLEX_UNIT_PX,tamanioLetra );
                    }
                    contenidoPalabra.setTypeface(robotoL);

                    List<String> listaRelacionados = new ArrayList<>();
                    List<String> listaAlternativos = new ArrayList<>();
                    listaFuentes = new HashMap<>();


                    for (String unStr : parts
                            ) {
                        if (unStr.contains("Relacionados")) {
                            String[] partes = unStr.split(Pattern.quote("|"));
                            for (Integer i = 1; i < partes.length - 1; i++) {
                                String elString = partes[i].replaceAll("[^a-zA-Z0-9áéíóú ]", "").substring(5).trim();

                                listaRelacionados.add(elString);
                            }
                        }
                        if (unStr.contains("TerminosAlternativos")) {
                            String[] partes = unStr.split(Pattern.quote("|"));
                            for (Integer i = 1; i < partes.length - 1; i++) {
                                String elString = partes[i].replaceAll("[^a-zA-Z0-9áéíóú ]", "").substring(3).trim();
                                listaAlternativos.add(elString);

                            }
                        }
                        if (unStr.contains("Fuentes")) {
                            String[] partes = unStr.split(Pattern.quote("|"));
                            for (Integer i = 1; i < partes.length - 1; i++) {

                                String[] subPartesKey = partes[i].split(Pattern.quote("="));
                                String[] subPartesValor = partes[i+1].split(Pattern.quote("="));

                                listaFuentes.put(subPartesKey[1].trim(),subPartesValor[1].trim());
                                i=i+1;
                            }
                        }
                    }
                    if (listaRelacionados.size() < 1) {
                        tituloPalabraRelacionadas.setVisibility(View.GONE);
                        recyclerTerminosRelacionados.setVisibility(View.GONE);
                    } else {
                        tituloPalabraRelacionadas.setVisibility(View.VISIBLE);
                        recyclerTerminosRelacionados.setVisibility(View.VISIBLE);

                        adapterRelacionados.setListaPalabrasOriginales(listaRelacionados);
                        adapterRelacionados.notifyDataSetChanged();
                    }

                    if (listaAlternativos.size() < 1) {

                        tituloAlternativos.setVisibility(View.GONE);
                        recyclerTerminosAlternativos.setVisibility(View.GONE);

                    } else {
                        tituloAlternativos.setVisibility(View.VISIBLE);
                        recyclerTerminosAlternativos.setVisibility(View.VISIBLE);
                        adapterAlternativos.setListaPalabrasOriginales(listaAlternativos);
                        adapterAlternativos.notifyDataSetChanged();

                    }

                    if (listaFuentes.size() < 1) {

                        tituloFuentes.setVisibility(View.GONE);
                        recyclerFuentes.setVisibility(View.GONE);

                    } else {
                        tituloFuentes.setVisibility(View.VISIBLE);
                        recyclerFuentes.setVisibility(View.VISIBLE);
                        List<String>nuevaListaFuentes=new ArrayList<>();
                        nuevaListaFuentes.addAll(listaFuentes.keySet());
                        adapterFuentes.setListaPalabrasOriginales(nuevaListaFuentes);
                        adapterFuentes.notifyDataSetChanged();

                    }
                }
                    else {

                        tituloPalabra.setText(laPalabra);
                        contenidoPalabra.setText("No se encontraron resultados para: "+laPalabra);
                        tituloAlternativos.setVisibility(View.GONE);
                        tituloAlternativos.setVisibility(View.GONE);
                    }
                    progressBar.setVisibility(View.GONE);
                }


            },laPalabra);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.relacionable = (Relacionable)context;
    }
}
