package com.martirio.delsectorsocial.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by elmar on 22/4/2018.
 */

public class PaginaCategorias {
    @SerializedName("limits")
    @Expose
    private QueryContinue limits;

    @SerializedName("query")
    @Expose
    private Query query;

    public QueryContinue getLimits() {
        return limits;
    }

    public void setLimits(QueryContinue limits) {
        this.limits = limits;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }
}
