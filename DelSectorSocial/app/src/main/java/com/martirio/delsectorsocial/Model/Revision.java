package com.martirio.delsectorsocial.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by elmar on 13/4/2018.
 */

public class Revision {
    @SerializedName("contentformat")
    @Expose
    private String pageid;

    @SerializedName("contentmodel")
    @Expose
    private String ns;

    @SerializedName("*")
    @Expose
    private String contenido;

    public String getPageid() {
        return pageid;
    }

    public void setPageid(String pageid) {
        this.pageid = pageid;
    }

    public String getNs() {
        return ns;
    }

    public void setNs(String ns) {
        this.ns = ns;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
}
