package com.martirio.delsectorsocial.View.Controller;

import android.content.Context;

import com.martirio.delsectorsocial.DAO.DAOinet;
import com.martirio.delsectorsocial.Model.PaginaWiki;
import com.martirio.delsectorsocial.Utils.HTTPConnectionManager;
import com.martirio.delsectorsocial.Utils.ResultListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elmar on 14/4/2018.
 */

public class ControllerDatos {
    private Context context;

    public ControllerDatos(Context context) {
    this.context=context;
    }


    public List<String> dameTitulos() {
        List<String> lista=new ArrayList<>();
        lista.add("inicio");
        lista.add("categorias");
        lista.add("Sugerir Palabra");
        return lista;
    }

    public void traerDatosNumeroPAgina(final ResultListener<PaginaWiki> listenerFromView, Integer idPagina){
        if (HTTPConnectionManager.isNetworkingOnline(context)) {

            DAOinet daoInet = new DAOinet();
            daoInet.traerDatosConNumeroPagina(new ResultListener<PaginaWiki>() {
                @Override
                public void finish(PaginaWiki unaPagina) {

                    listenerFromView.finish(unaPagina);

                }
            }, idPagina);

        }
        else{

        }
    }

    public void traerTitulosConPalabra(final ResultListener<PaginaWiki> listenerFromView, String palabra){
        if (HTTPConnectionManager.isNetworkingOnline(context)) {

            DAOinet daoInet = new DAOinet();
            daoInet.traerTitulosConPalabra(new ResultListener<PaginaWiki>() {
                @Override
                public void finish(PaginaWiki unaPagina) {

                    listenerFromView.finish(unaPagina);

                }
            }, palabra);

        }
        else{

        }
    }
    public void traerTitulosConPalabra(final ResultListener<PaginaWiki> listenerFromView, String palabra, String categoria){
        if (HTTPConnectionManager.isNetworkingOnline(context)) {

            DAOinet daoInet = new DAOinet();
            daoInet.traerTitulosConPalabra(new ResultListener<PaginaWiki>() {
                @Override
                public void finish(PaginaWiki unaPagina) {

                    listenerFromView.finish(unaPagina);

                }
            }, palabra,categoria);

        }
        else{

        }
    }

    public void traerDatosConTituloExacto(final ResultListener<PaginaWiki> listenerFromView, String titulo){
        if (HTTPConnectionManager.isNetworkingOnline(context)) {

            DAOinet daoInet = new DAOinet();
            daoInet.traerDatosConTitulo(new ResultListener<PaginaWiki>() {
                @Override
                public void finish(PaginaWiki unaPagina) {

                    listenerFromView.finish(unaPagina);

                }
            }, titulo);

        }
        else{

        }
    }


    public void traerCategorias(final ResultListener<PaginaWiki> resultListener, String laPalabra) {
        if (HTTPConnectionManager.isNetworkingOnline(context)) {

            DAOinet daoInet = new DAOinet();
            daoInet.traerCategorias(new ResultListener<PaginaWiki>() {
                @Override
                public void finish(PaginaWiki unaPagina) {

                    resultListener.finish(unaPagina);

                }
            }, laPalabra);

        }
        else{

        }
    }

    public void traerPalabrasConCategorias(final ResultListener<PaginaWiki> resultListener, String laCategoria) {
        if (HTTPConnectionManager.isNetworkingOnline(context)) {

            DAOinet daoInet = new DAOinet();
            daoInet.traerPalabrasConCategorias(new ResultListener<PaginaWiki>() {
                @Override
                public void finish(PaginaWiki unaPagina) {

                    resultListener.finish(unaPagina);

                }
            }, laCategoria);

        }
        else{

        }
    }
}
