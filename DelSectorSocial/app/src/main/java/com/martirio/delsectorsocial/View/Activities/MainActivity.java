package com.martirio.delsectorsocial.View.Activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.martirio.delsectorsocial.DAO.DAOinet;
import com.martirio.delsectorsocial.Model.Categoria;
import com.martirio.delsectorsocial.Model.Page;
import com.martirio.delsectorsocial.Model.PaginaWiki;
import com.martirio.delsectorsocial.R;
import com.martirio.delsectorsocial.Utils.HTTPConnectionManager;
import com.martirio.delsectorsocial.Utils.ResultListener;
import com.martirio.delsectorsocial.View.Adapter.AdapterPagerFragment;
import com.martirio.delsectorsocial.View.Controller.ControllerDatos;
import com.martirio.delsectorsocial.View.Fragments.FragmentCategorias;
import com.martirio.delsectorsocial.View.Fragments.InicioFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MainActivity extends AppCompatActivity implements InicioFragment.Avisable, FragmentCategorias.Categorizable{

    private TextView contenido;
    private TextView titulo;
    private Button boton;
    private EditText pagina;

    private TabLayout tabLayout;
    private ViewPager pager;
    private AdapterPagerFragment adapterPagerFragment;

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;

    private ControllerDatos controllerDatos;

    private Toolbar toolbar;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    
    private SharedPreferences config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout=findViewById(R.id.tabLayout);
        pager=findViewById(R.id.viewPagerAuditoria);

        drawerLayout =  findViewById(R.id.elDrawer);
        navigationView = findViewById(R.id.naview);


        //PIDO DATOS Y CARGO EL ADAPTER AL PAGER
        controllerDatos=new ControllerDatos(this);
        adapterPagerFragment=new AdapterPagerFragment(getSupportFragmentManager(),controllerDatos.dameTitulos());
        pager.setAdapter(adapterPagerFragment);
        adapterPagerFragment.notifyDataSetChanged();

        View headerLayout = navigationView.getHeaderView(0);

        toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        Typeface robotoR = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        TextView unText=toolbar.findViewById(R.id.textoToolbar);
        unText.setTypeface(robotoR);
        unText.setTextColor(getResources().getColor(R.color.blancoNomad));

        unText.setText(getResources().getString(R.string.delSectorSocial));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        actionBarDrawerToggle.getDrawerArrowDrawable().setColor(ContextCompat.getColor(this, R.color.blancoNomad));


        drawerLayout.addDrawerListener(actionBarDrawerToggle);

//        ACCION DE LOS BOTONES DEL DRAWER
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.item11:
                            pager.setCurrentItem(0);
                            break;
                        case R.id.item12:
                            pager.setCurrentItem(1);
                            break;
                        case R.id.item13:
                            pager.setCurrentItem(2);
                            break;
                        case R.id.item14:
                            mostrarQuienesSomos("drawer");
                            break;
                        case R.id.item15:
                            cambiarPais();
                            break;
                    }

                drawerLayout.closeDrawers();
                return true;
            }
        });
//        SETEAR EL TABLAYOUT

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(pager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition(), true);
            }
        });

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });



    }

    private void cambiarPais() {
        config = this.getSharedPreferences("prefs",0);
        final String categoriaElegida =config.getString("categoriaElegida","Argentina");
        List<String> unaLista=new ArrayList<>();
        unaLista.add("Argentina");
        unaLista.add("Colombia");
        unaLista.add("España");
        unaLista.add("Francia");

        Integer paisSeleccionado=calcularPaisSeleccionado(categoriaElegida);

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Bienvenid@")
                .items(unaLista)
                .content(getResources().getString(R.string.elijaPais)+"\n"+getResources().getString(R.string.elijaPais2))
                .widgetColor(getResources().getColor(R.color.tile3))
                .cancelable(false)
                .itemsCallbackSingleChoice(paisSeleccionado, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, final int which, final CharSequence text) {

                        SharedPreferences.Editor editor = config.edit();
                        editor.putString("categoriaElegida",calcularTexto(text));
                        editor.putBoolean("esPrimeraVez",false);
                        editor.commit();
                        adapterPagerFragment.limpiarAdapters();
                        return true;

                    }
                })
                .positiveText("Listo!")
                .build();
        dialog.show();
    }

    private Integer calcularPaisSeleccionado(String categoriaElegida) {
        List<String>unaLista=new ArrayList<>();
        unaLista.add("Argentina");
        unaLista.add("Colombia");
        unaLista.add("España");
        unaLista.add("France");
        return unaLista.indexOf(categoriaElegida);
    }

    private String calcularTexto(CharSequence text) {
        String recibido=text.toString();
        String procesado;
        switch (recibido){
            case "Francia":
                procesado = "France";
                break;
            default:
                procesado= recibido;
                break;
        }
        return  procesado;
    }

    private void mostrarQuienesSomos(String origen) {
        Intent inten = new Intent(this,ActivityVerPalabra.class);
        Bundle bundle = new Bundle();
        bundle.putString(ActivityVerPalabra.ORIGEN,origen);
        bundle.putString(ActivityVerPalabra.NUMEROPAGINABUSCADA,null);
        bundle.putString(ActivityVerPalabra.PALABRABUSCADA,null);
        inten.putExtras(bundle);
        startActivity(inten);
    }

    @Override
    public void clickearPalabra(String unString, String origen) {
        Intent inten = new Intent(this,ActivityVerPalabra.class);
        Bundle bundle = new Bundle();
        bundle.putString(ActivityVerPalabra.ORIGEN,origen);
        bundle.putString(ActivityVerPalabra.NUMEROPAGINABUSCADA,null);
        bundle.putString(ActivityVerPalabra.PALABRABUSCADA,unString);
        inten.putExtras(bundle);
        startActivity(inten);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void clickearCategoria(Page unaCat, String elOrigen) {
        if (HTTPConnectionManager.isNetworkingOnline(this)){
            String stringCaT= unaCat.getTitle().replace(" ","_");
            stringCaT=stringCaT.replace("Categoría:","");
            clickearPalabra(stringCaT,elOrigen);
        }
        else{
            Toast.makeText(this, getResources().getString(R.string.sinConexion), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void clickearCategoria(String unString, String origen) {
        if (HTTPConnectionManager.isNetworkingOnline(this)){
            clickearPalabra(unString,origen);
        }
        else{
            Toast.makeText(this, getResources().getString(R.string.sinConexion), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean estaAbiertoAboutus() {
        FragmentManager fragmentManager=this.getFragmentManager();
        Fragment elFragment = fragmentManager.findFragmentByTag("fragmentAboutUs");
        if (elFragment!=null && elFragment.isVisible()){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void limpiarAdapters() {
        adapterPagerFragment.limpiarAdapters();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
