package com.martirio.delsectorsocial.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by elmar on 13/4/2018.
 */

public class Page {

    @SerializedName("pageid")
    @Expose
    private String pageid;

    @SerializedName("ns")
    @Expose
    private String ns;

    @SerializedName("title")
    @Expose
    private String title;


    @SerializedName("revisions")
    @Expose
    private List<Revision> revisions;


    public String getPageid() {
        return pageid;
    }

    public void setPageid(String pageid) {
        this.pageid = pageid;
    }

    public String getNs() {
        return ns;
    }

    public void setNs(String ns) {
        this.ns = ns;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Revision> getRevisions() {
        return revisions;
    }

    public void setRevisions(List<Revision> revisions) {
        this.revisions = revisions;
    }
}
