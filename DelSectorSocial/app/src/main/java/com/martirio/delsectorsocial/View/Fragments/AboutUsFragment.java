package com.martirio.delsectorsocial.View.Fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.martirio.delsectorsocial.R;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment {


    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);

        TextView linkCeroDerecha = view.findViewById(R.id.linkCeroALaDerecha);
        TextView mailPersona1 = view.findViewById(R.id.persona1);
        TextView mailPersona2 = view.findViewById(R.id.persona3);
        TextView mailPersona3 = view.findViewById(R.id.persona5);

        TextView calle = view.findViewById(R.id.calle);
        TextView sustancia = view.findViewById(R.id.sustancias);
        TextView niniez = view.findViewById(R.id.conceptoSencillo);
        TextView discapacidad = view.findViewById(R.id.discapacidad);

        TextView martin= view.findViewById(R.id.nombreRiso);

        mailPersona1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mandarMail("marcela@delsectorosocial.org");
            }
        });
        mailPersona2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mandarMail("alejandra@delsectorsocial.org");
            }
        });
        mailPersona3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mandarMail("berangerehcastera@hotmail.fr");
            }
        });
        martin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mandarMail("risomartin@gmail.com");
            }
        });



        linkCeroDerecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.ceroaladerecha.org/"));
                getActivity().startActivity(intent);
            }
        });

        sustancia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.argentina.gob.ar/sedronar"));
                getActivity().startActivity(intent);
            }
        });

        calle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/elpobredeasis/"));
                getActivity().startActivity(intent);
            }
        });

        discapacidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.lausina.org/"));
                getActivity().startActivity(intent);
            }
        });

        niniez.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/ConceptosSencillos?fref=ts"));
                getActivity().startActivity(intent);
            }
        });

        return view;
    }
    public void mandarMail(String aQuien) {

            try {

                String recepientEmail = ""; // either set to destination email or leave empty
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:" + aQuien));
                startActivity(intent);


            } catch (Exception e) {
                Log.e("NOMAD/ERROR", "exception", e);
            }

    }

}
