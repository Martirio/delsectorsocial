package com.martirio.delsectorsocial.View.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.martirio.delsectorsocial.R;

import java.util.List;


/**
 * Created by elmar on 18/5/2017.
 */

public class AdapterPalabrasFuentes extends RecyclerView.Adapter implements View.OnClickListener, View.OnLongClickListener {

    private Context context;
    private List<String> listaPalabrasOriginales;
    private View.OnClickListener listener;
    private AdapterView.OnLongClickListener listenerLong;


    public void setLongListener(View.OnLongClickListener unLongListener) {
        this.listenerLong = unLongListener;
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListaPalabrasOriginales(List<String> listaPalabrasOriginales) {
        this.listaPalabrasOriginales = listaPalabrasOriginales;
    }

    public void addListaAreasOriginales(List<String> listaAreasOriginales) {
        this.listaPalabrasOriginales.addAll(listaAreasOriginales);
    }


    public List<String> getListaPalabrasOriginales() {
        return listaPalabrasOriginales;
    }

    //crear vista y viewholder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View viewCelda;

            viewCelda = layoutInflater.inflate(R.layout.detalle_celda_listado_palabras, parent, false);


        viewCelda.setOnClickListener(this.listener);

        return new AreaViewHolder(viewCelda);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final String unArea = listaPalabrasOriginales.get(position);
        AreaViewHolder AreaViewHolder = (AreaViewHolder) holder;
        AreaViewHolder.cargarArea(unArea);

    }

    @Override
    public int getItemCount() {
        return listaPalabrasOriginales.size();
    }

    @Override
    public void onClick(View view) {
        listener.onClick(view);
    }

    @Override
    public boolean onLongClick(View v) {
        listenerLong.onLongClick(v);
        return true;
    }

    //creo el viewholder que mantiene las referencias
    //de los elementos de la celda

    private static class AreaViewHolder extends RecyclerView.ViewHolder {

        private TextView textoCeldaRecycler;
        private TextView textoCantidadPaginas;


        public AreaViewHolder(View itemView) {
            super(itemView);

            textoCeldaRecycler = itemView.findViewById(R.id.textoInteriorRecycler);
            textoCantidadPaginas = itemView.findViewById(R.id.cantidadPaginas);

        }

        public void cargarArea(String palabra) {

            Typeface robotoL = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/Roboto-Light.ttf");
            textoCeldaRecycler.setTypeface(robotoL);
            textoCeldaRecycler.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            textoCeldaRecycler.setText(palabra);

        }


    }

}
