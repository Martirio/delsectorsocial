package com.martirio.delsectorsocial.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * Created by elmar on 13/4/2018.
 */

public class Query {

    @SerializedName("pages")
    @Expose
    private Map<String, Page> pages;

    @SerializedName("categorymembers")
    @Expose
    private List<Page> pagesCat;

    @SerializedName("allcategories")
    @Expose
    private List<Categoria> categoList;

    /**
     *
     * @return
     * The feeds
     */
    public Map<String, Page>  getPages() {
        return pages;
    }
    /**
     *
     * @param pages
     * The feeds
     */
    public void setFeeds(Map<String, Page>  pages) {
        this.pages = pages;
    }

    public List<Page> getPagesCat() {
        return pagesCat;
    }

    public void setPagesCat(List<Page> pagesCat) {
        this.pagesCat = pagesCat;
    }

    public List<Categoria> getCategoList() {
        return categoList;
    }

    public void setCategoList(List<Categoria> categoList) {
        this.categoList = categoList;
    }
}
