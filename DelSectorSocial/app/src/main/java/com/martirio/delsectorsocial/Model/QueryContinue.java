package com.martirio.delsectorsocial.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by elmar on 16/4/2018.
 */

class QueryContinue {
    @SerializedName("allpages")
    @Expose
    private AllPages allPages;
}
