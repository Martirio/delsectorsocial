package com.martirio.delsectorsocial.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by elmar on 13/4/2018.
 */

public class PaginaWiki {

    @SerializedName("query-continue")
    @Expose
    private QueryContinue queryContinue;

    @SerializedName("query")
    @Expose
    private Query query;
    /**
     *
     * @return
     * The data
     */
    public Query getData() {
        return query;
    }
    /**
     *
     * @param data
     * The data
     */
    public void setData(Query data) {
        this.query = data;
    }

}
