package com.martirio.delsectorsocial.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by elmar on 16/4/2018.
 */

public class AllPages {


    @SerializedName("gapcontinue")
    @Expose
    private String palabraContinua;

    public String getPalabraContinua() {
        return palabraContinua;
    }

    public void setPalabraContinua(String palabraContinua) {
        this.palabraContinua = palabraContinua;
    }
}