package com.martirio.delsectorsocial.View.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.martirio.delsectorsocial.Model.Page;
import com.martirio.delsectorsocial.R;

import java.util.List;


/**
 * Created by elmar on 18/5/2017.
 */

public class AdapterPages extends RecyclerView.Adapter implements View.OnClickListener, View.OnLongClickListener {

    private Context context;
    private List<Page> listaPalabrasOriginales;
    private View.OnClickListener listener;
    private AdapterView.OnLongClickListener listenerLong;




    public void setLongListener(View.OnLongClickListener unLongListener) {
        this.listenerLong = unLongListener;
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListaPalabrasOriginales(List<Page> listaPalabrasOriginales) {
        this.listaPalabrasOriginales = listaPalabrasOriginales;
    }

    public void addListaAreasOriginales(List<Page> listaAreasOriginales) {
        this.listaPalabrasOriginales.addAll(listaAreasOriginales);
    }


    public List<Page> getListaPalabrasOriginales() {
        return listaPalabrasOriginales;
    }

    //crear vista y viewholder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View viewCelda;

            viewCelda = layoutInflater.inflate(R.layout.detalle_celda_pages, parent, false);

            viewCelda.setOnClickListener(listener);


        return new AreaViewHolder(viewCelda);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Page unArea = listaPalabrasOriginales.get(position);
        AreaViewHolder AreaViewHolder = (AreaViewHolder) holder;
        AreaViewHolder.cargarArea(unArea);

    }

    @Override
    public int getItemCount() {
        return listaPalabrasOriginales.size();
    }

    @Override
    public void onClick(View view) {
        listener.onClick(view);
    }

    @Override
    public boolean onLongClick(View v) {
        listenerLong.onLongClick(v);
        return true;
    }

    //creo el viewholder que mantiene las referencias
    //de los elementos de la celda

    private static class AreaViewHolder extends RecyclerView.ViewHolder {

        private TextView textoCeldaRecycler;
        private TextView textoCantidadPaginas;


        public AreaViewHolder(View itemView) {
            super(itemView);

            textoCeldaRecycler = itemView.findViewById(R.id.textoInteriorRecycler);
            textoCantidadPaginas = itemView.findViewById(R.id.cantidadPaginas);

        }

        public void cargarArea(Page palabra) {

            Typeface robotoL = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/Roboto-Light.ttf");
            Typeface robotoB = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/Roboto-Bold.ttf");
            textoCeldaRecycler.setTypeface(robotoL);
            textoCantidadPaginas.setTypeface(robotoB);
            String elTituloCategoria=palabra.getTitle().replaceAll("Categoría:","");
            textoCeldaRecycler.setText(elTituloCategoria);


        }


    }

}
