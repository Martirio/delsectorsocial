package com.martirio.delsectorsocial.Utils;

/**
 * Created by elmar on 14/4/2018.
 */

public class APIHELPER {


    public static String buscarPorNumeroPagina(String numeroPagina){
     return "https://delsectorsocial.org/api.php?action=query&prop=revisions&rvprop=content&format=json&pageids="+numeroPagina;
    }
    public static String buscarPorPalabra(String palabra){
       // https://delsectorsocial.org/api.php?action=query&generator=allpages&format=json&gapfrom=fa
        //https://delsectorsocial.org/api.php?action=query&list=categorymembers&cmsort=sortkey&cmtitle=Category:argentina&format=json&&cmstartsortkeyprefix=fa
        return "https://delsectorsocial.org/api.php?action=query&generator=allpages&format=json&gapfrom="+palabra;
    }
    public static String buscarPorPalabra(String palabra, String categoria){
       return "https://delsectorsocial.org/api.php?action=query&list=categorymembers&cmsort=sortkey&cmtitle=Category:"+categoria+"&format=json&cmlimit=25&cmstartsortkeyprefix="+palabra;
    }

    public static String traerTitulosParaPalabra(String palabra){
//        https://delsectorsocial.org/api.php?action=query&list=allpages&cmsort=sortkey&format=json&cmlimit=20&cmstartsortkeyprefix=
        return "https://delsectorsocial.org/api.php?action=query&list=allpages&aplimit=12&format=json&apfrom="+palabra;
    }

    public static String buscarPorTituloExacto(String titulo){
       // https://delsectorsocial.org/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Abuelos
        return "https://delsectorsocial.org/api.php?action=query&prop=revisions&rvprop=content&format=json&titles="+titulo;
    }


    public static String buscarVinculosExternos(String tituloPAgina){
        //https://delsectorsocial.org/api.php?action=query&prop=extlinks&format=json&titles=Pa%C3%ADses_subdesarrollados
       return "https://delsectorsocial.org/api.php?action=query&prop=extlinks&format=json&titles="+tituloPAgina;
    }

    public static String buscarVinculosExternosPorNumeroDePAgina(String numeroDePagina){
        //https://delsectorsocial.org/api.php?action=query&prop=extlinks&format=json&pageids=247
        return "https://delsectorsocial.org/api.php?action=query&prop=extlinks&format=json&pageids="+numeroDePagina;
    }

    public static String traerCategorias(String pais){

        //viejoviejo//https://delsectorsocial.org/api.php?action=query&list=allcategories&aclimit=max&format=json&acprop=size
        //viejo//https://delsectorsocial.org/api.php?action=query&list=categorymembers&cmtype=subcat&format=json&cmlimit=400&cmtitle=Category:Principal
        //nuevo//https://delsectorsocial.org/api.php?action=query&list=categorymembers&cmtype=subcat&format=json&cmlimit=400&cmtitle=Category:Argentina
        return "https://delsectorsocial.org/api.php?action=query&list=categorymembers&cmtype=subcat&format=json&cmlimit=400&cmtitle=Category:"+pais;
    }

    public static String traerPalabrasCategoria(String categoria){
        //https://delsectorsocial.org/api.php?action=query&list=allcategories&aclimit=max&format=json&acprop=size
        return "https://delsectorsocial.org/api.php?action=query&list=categorymembers&format=json&cmlimit=max&cmtitle=Category:"+categoria;
    }

}
