package com.martirio.delsectorsocial.Utils;

public interface ResultListener<T> {

    public void finish(T resultado);
}