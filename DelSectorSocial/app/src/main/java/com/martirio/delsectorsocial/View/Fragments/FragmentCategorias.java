package com.martirio.delsectorsocial.View.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martirio.delsectorsocial.Model.Page;
import com.martirio.delsectorsocial.Model.PaginaWiki;
import com.martirio.delsectorsocial.R;
import com.martirio.delsectorsocial.Utils.ResultListener;
import com.martirio.delsectorsocial.View.Adapter.AdapterPages;
import com.martirio.delsectorsocial.View.Adapter.AdapterPalabrasListado;
import com.martirio.delsectorsocial.View.Controller.ControllerDatos;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */

public class FragmentCategorias extends Fragment {


    private List<String> listaPalabras;
    private Categorizable categorizable;
    private String pais;

    public FragmentCategorias() {
        // Required empty public constructor
    }
    private RecyclerView recyclerCategoria;
    private AdapterPalabrasListado adapterPalabrasListado;
    private AdapterPages adapterPages;
    private ControllerDatos controllerDatos;

    private SharedPreferences config;

    public void limpiarAdapter() {
        final String paisElegido=config.getString("categoriaElegida","Argentina");


        controllerDatos=new ControllerDatos(getContext());
        controllerDatos.traerCategorias(new ResultListener<PaginaWiki>() {
            @Override
            public void finish(PaginaWiki resultado) {


                if (resultado!=null && resultado.getData()!=null && resultado.getData().getPagesCat()!=null) {
                    actualizarRecycler(resultado.getData().getPagesCat());
                }

            }
        },paisElegido);
    }

    public interface Categorizable {
        void clickearCategoria(Page unaCat, String elOrigen);
        void clickearCategoria(String unaCat, String elOrigen);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_categorias, container, false);

        if (getActivity()!=null) {
            config = getActivity().getSharedPreferences("prefs",0);
            pais =config.getString("categoriaElegida","Argentina");
        }

        recyclerCategoria= view.findViewById(R.id.recyclerCategorias);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerCategoria.setLayoutManager(linearLayoutManager);

        adapterPages =new AdapterPages();
        adapterPages.setContext(getContext());
        adapterPages.setListaPalabrasOriginales(new ArrayList<Page>());
        recyclerCategoria.setAdapter(adapterPages);
        adapterPages.notifyDataSetChanged();

      
        adapterPages.notifyDataSetChanged();

        View.OnClickListener listener=new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Integer posicion=recyclerCategoria.getChildAdapterPosition(view);
                    List<Page> unaLista= adapterPages.getListaPalabrasOriginales();
                    Page unaCat=unaLista.get(posicion);
                    categorizable.clickearCategoria(unaCat, "categoria");

            }
        };
        adapterPages.setListener(listener);

        traerLasCategorias();

        return view;
    }

    private void traerLasCategorias() {
        final String paisElegido=config.getString("categoriaElegida","Argentina");


            controllerDatos=new ControllerDatos(getContext());
            controllerDatos.traerCategorias(new ResultListener<PaginaWiki>() {
                @Override
                public void finish(PaginaWiki resultado) {


                    if (resultado!=null && resultado.getData()!=null && resultado.getData().getPagesCat()!=null) {
                        actualizarRecycler(resultado.getData().getPagesCat());
                    }

                }
            },paisElegido);
            

    }



    private void actualizarRecycler(List<Page> listaPalabras) {
       
        this.adapterPages.setListaPalabrasOriginales(listaPalabras);
        this.adapterPages.notifyDataSetChanged();
    }


    public static FragmentCategorias crearListadoFragment(){
        return new FragmentCategorias();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.categorizable=(Categorizable)context;
    }
}
