package com.martirio.delsectorsocial.DAO;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.martirio.delsectorsocial.Model.PaginaCategorias;
import com.martirio.delsectorsocial.Model.PaginaWiki;
import com.martirio.delsectorsocial.Utils.APIHELPER;
import com.martirio.delsectorsocial.Utils.HTTPConnectionManager;
import com.martirio.delsectorsocial.Utils.ResultListener;

/**
 * Created by elmar on 9/4/2018.
 */

public class DAOinet {

    String urlParaAsyncTask;


    //PELICULAS POPULARES
    public void traerDatosConNumeroPagina(ResultListener<PaginaWiki> listenerFromController, Integer idPagina){

        //cargo el string de la busqueda con la que le pego al api
        this.urlParaAsyncTask = APIHELPER.buscarPorNumeroPagina(idPagina.toString());
        //LE ESTOY INDICANDO AL DAO QUE EJECUTE LA TAREA EN SEGUNDO PLANO
        ObtenerUnFormatoTask tarea = new ObtenerUnFormatoTask();
        tarea.setListenerFromController(listenerFromController);
        tarea.execute();
    }

    public void traerDatosConPalabra(ResultListener<PaginaWiki> listenerFromController, String palabra){

        //cargo el string de la busqueda con la que le pego al api
        this.urlParaAsyncTask = APIHELPER.buscarPorPalabra(palabra);
        //LE ESTOY INDICANDO AL DAO QUE EJECUTE LA TAREA EN SEGUNDO PLANO
        ObtenerUnFormatoTask tarea = new ObtenerUnFormatoTask();
        tarea.setListenerFromController(listenerFromController);
        tarea.execute();
    }

    public void traerDatosConTitulo(ResultListener<PaginaWiki> listenerFromController, String titulo) {

        String unString = titulo.replaceAll(" ", "_");
        //cargo el string de la busqueda con la que le pego al api
        this.urlParaAsyncTask = APIHELPER.buscarPorTituloExacto(unString);
        //LE ESTOY INDICANDO AL DAO QUE EJECUTE LA TAREA EN SEGUNDO PLANO
        ObtenerUnFormatoTask tarea = new ObtenerUnFormatoTask();
        tarea.setListenerFromController(listenerFromController);
        tarea.execute();
    }


    public void traerTitulosConPalabra(ResultListener<PaginaWiki> resultListener, String palabra) {
        //cargo el string de la busqueda con la que le pego al api
        this.urlParaAsyncTask = APIHELPER.traerTitulosParaPalabra(palabra);
        //LE ESTOY INDICANDO AL DAO QUE EJECUTE LA TAREA EN SEGUNDO PLANO
        ObtenerUnFormatoTask tarea = new ObtenerUnFormatoTask();
        tarea.setListenerFromController(resultListener);
        tarea.execute();
    }
    public void traerTitulosConPalabra(ResultListener<PaginaWiki> resultListener, String palabra,String categoria) {
        //cargo el string de la busqueda con la que le pego al api
        this.urlParaAsyncTask = APIHELPER.buscarPorPalabra(palabra,categoria);
        //LE ESTOY INDICANDO AL DAO QUE EJECUTE LA TAREA EN SEGUNDO PLANO
        ObtenerUnFormatoTask tarea = new ObtenerUnFormatoTask();
        tarea.setListenerFromController(resultListener);
        tarea.execute();
    }

    public void traerCategorias(ResultListener<PaginaWiki> resultListener, String laPalabra) {
        //cargo el string de la busqueda con la que le pego al api
        this.urlParaAsyncTask = APIHELPER.traerCategorias(laPalabra);
        //LE ESTOY INDICANDO AL DAO QUE EJECUTE LA TAREA EN SEGUNDO PLANO
        ObtenerUnFormatoTask tarea = new ObtenerUnFormatoTask();
        tarea.setListenerFromController(resultListener);
        tarea.execute();
    }

    public void traerPalabrasConCategorias(ResultListener<PaginaWiki> resultListener, String laCategoria) {
        //cargo el string de la busqueda con la que le pego al api
        this.urlParaAsyncTask = APIHELPER.traerPalabrasCategoria(laCategoria);
        //LE ESTOY INDICANDO AL DAO QUE EJECUTE LA TAREA EN SEGUNDO PLANO
        ObtenerUnFormatoTask tarea = new ObtenerUnFormatoTask();
        tarea.setListenerFromController(resultListener);
        tarea.execute();
    }




    private class ObtenerUnFormatoTask extends AsyncTask<String,Void,PaginaWiki>{

        private ResultListener<PaginaWiki> listenerFromController;

        public void setListenerFromController(ResultListener<PaginaWiki> listenerFromController) {
            this.listenerFromController = listenerFromController;
        }

        @Override
        protected PaginaWiki doInBackground(String... params) {

            PaginaWiki Palabras = new PaginaWiki();
            try {
                //PEDIR A INTERNET USANDO UNA URL EL ARCHIVO JSON
                HTTPConnectionManager httpConnectionManager = new HTTPConnectionManager();
                String json = httpConnectionManager.getRequestString(urlParaAsyncTask);

                //USAR GSON PARA PARSEAR EL ARCHIVO Y CONVERTIRLO A LA LISTA DE NOTICIAS
                Gson gson = new Gson();
                PaginaWiki PaginaWiki = gson.fromJson(json, PaginaWiki.class);
                Palabras = PaginaWiki;

            }
            catch (Exception e){
                e.printStackTrace();
            }


            //DEVOLVER LA LISTA
            return Palabras;
        }

        @Override
        protected void onPostExecute(PaginaWiki Palabras) {

            //AVISARLE AL CONTROLLER QUE SU LISTA DE NOTICIAS ESTA CARGADA
            listenerFromController.finish(Palabras);
        }
    }


    private class TraerCategoria extends AsyncTask<String,Void,PaginaCategorias>{

        private ResultListener<PaginaCategorias> listenerFromController;

        public void setListenerFromController(ResultListener<PaginaCategorias> listenerFromController) {
            this.listenerFromController = listenerFromController;
        }

        @Override
        protected PaginaCategorias doInBackground(String... params) {

            PaginaCategorias Palabras = new PaginaCategorias();
            try {
                //PEDIR A INTERNET USANDO UNA URL EL ARCHIVO JSON
                HTTPConnectionManager httpConnectionManager = new HTTPConnectionManager();
                String json = httpConnectionManager.getRequestString(urlParaAsyncTask);

                //USAR GSON PARA PARSEAR EL ARCHIVO Y CONVERTIRLO A LA LISTA DE NOTICIAS
                Gson gson = new Gson();
                PaginaCategorias PaginaCategorias = gson.fromJson(json, PaginaCategorias.class);
                Palabras = PaginaCategorias;

            }
            catch (Exception e){
                e.printStackTrace();
            }


            //DEVOLVER LA LISTA
            return Palabras;
        }

        @Override
        protected void onPostExecute(PaginaCategorias Palabras) {

            //AVISARLE AL CONTROLLER QUE SU LISTA DE NOTICIAS ESTA CARGADA
            listenerFromController.finish(Palabras);
        }
    }



}
