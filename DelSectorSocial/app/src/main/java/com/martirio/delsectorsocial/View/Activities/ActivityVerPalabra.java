package com.martirio.delsectorsocial.View.Activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.martirio.delsectorsocial.R;
import com.martirio.delsectorsocial.View.Controller.ControllerDatos;
import com.martirio.delsectorsocial.View.Fragments.AboutUsFragment;
import com.martirio.delsectorsocial.View.Fragments.FragmentMostrarPalabra;
import com.martirio.delsectorsocial.View.Fragments.InicioFragment;
import com.martirio.delsectorsocial.View.Fragments.ListadoFragment;
import com.martirio.delsectorsocial.View.Fragments.SugerirFragment;

public class ActivityVerPalabra extends AppCompatActivity implements FragmentMostrarPalabra.Relacionable, ListadoFragment.Listable{

    public static final String ORIGEN="ORIGEN";
    public static final String PALABRABUSCADA="PLABRABUSCADA";
    public static final String NUMEROPAGINABUSCADA="NUMEROPAGINABUSCADA";

    private String elOrigen;
    private String laPalabra;
    private String elNumeroPagina;

    private ControllerDatos controllerDatosVerPalabra;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
private NavigationView navigationView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_palabra);

        controllerDatosVerPalabra = new ControllerDatos(this);

        //DEJAMOS EL DRAWER INACTIVO
            /*
            drawerLayout =  findViewById(R.id.elDrawer);
            navigationView = findViewById(R.id.naview);
            */


        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();

        if (bundle!=null){
            elOrigen=bundle.getString(ORIGEN);
            laPalabra=bundle.getString(PALABRABUSCADA);
            elNumeroPagina=bundle.getString(NUMEROPAGINABUSCADA);
        }

        switch (elOrigen){
            case "busqueda":
                traerTextoPalabra(laPalabra,elOrigen);
                break;
            case "buscada":
                traerPalabrasBuscadas(laPalabra,elOrigen);
                break;
            case "categoria":
                traerPalabrasCategoria(laPalabra,elOrigen);
                break;
            case "drawer":
                abrirAboutUs();
            default:
                break;
        }



        //TOOLBAR-START
        /* //DRAWER INACTIVO
        View headerLayout = navigationView.getHeaderView(0);
        */
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        Typeface robotoR = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        TextView unText= toolbar.findViewById(R.id.textoToolbar);
        unText.setTypeface(robotoR);
        unText.setTextColor(getResources().getColor(R.color.blancoNomad));
        unText.setText("Del Sector Social");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //        TOGGLE PARA EL BOTON HAMBURGUESA

        //TOOLBAR-END

        //        ACCION DE LOS BOTONES DEL DRAWER

        /*
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.item11:

                        break;
                    case R.id.item12:

                        break;
                    case R.id.item13:

                        break;
                    case R.id.item14:

                        break;

                }

                drawerLayout.closeDrawers();
                return true;
            }
        });
        */

    }

    private void traerPalabrasBuscadas(String laPalabra, String elOrigen) {
        ListadoFragment fragmentMostrarPalabra = new ListadoFragment();
        Bundle elBundle= new Bundle();
        elBundle.putString(ListadoFragment.ORIGEN, elOrigen);
        elBundle.putString(ListadoFragment.PALABRABUSCADA, laPalabra);
        fragmentMostrarPalabra.setArguments(elBundle);
        android.support.v4.app.FragmentManager fragmentManager =getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerFragmentPalabra, fragmentMostrarPalabra, "fragmentCategoria");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void abrirAboutUs() {
        AboutUsFragment aboutUsFragment = new AboutUsFragment();
        Bundle elBundle= new Bundle();
        elBundle.putString(ListadoFragment.ORIGEN,"drawer");
        aboutUsFragment.setArguments(elBundle);
        android.support.v4.app.FragmentManager fragmentManager =getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerFragmentPalabra, aboutUsFragment, "fragmentAboutUs");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void traerPalabrasCategoria(String laCategoria, String elOrigen) {
        ListadoFragment fragmentMostrarPalabra = new ListadoFragment();
        Bundle elBundle= new Bundle();
        elBundle.putString(ListadoFragment.ORIGEN, elOrigen);
        elBundle.putString(ListadoFragment.PALABRABUSCADA, laCategoria);
        fragmentMostrarPalabra.setArguments(elBundle);
        android.support.v4.app.FragmentManager fragmentManager =getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerFragmentPalabra, fragmentMostrarPalabra, "fragmentCategoria");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    private void traerTextoPalabra(String laPalabra, String elOrigen) {
        FragmentMostrarPalabra fragmentMostrarPalabra = new FragmentMostrarPalabra();
        Bundle elBundle= new Bundle();
        elBundle.putString(FragmentMostrarPalabra.ORIGEN, elOrigen);
        elBundle.putString(FragmentMostrarPalabra.NUMEROPAGINABUSCADA, elNumeroPagina);
        elBundle.putString(FragmentMostrarPalabra.PALABRABUSCADA, laPalabra);
        fragmentMostrarPalabra.setArguments(elBundle);
        android.support.v4.app.FragmentManager fragmentManager =getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.containerFragmentPalabra, fragmentMostrarPalabra, "fragmentMostrarPalabra");
        fragmentTransaction.addToBackStack("asd");
        fragmentTransaction.commit();
    }


    @Override
    public void abrirTerminoRelacionado(String terminoRelacionado) {
        traerTextoPalabra(terminoRelacionado,"busqueda");
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.FragmentManager elfra=this.getSupportFragmentManager();
        android.support.v4.app.Fragment elFragment=elfra.findFragmentByTag("fragmentMostrarPalabra");
        android.support.v4.app.Fragment elFragment2=elfra.findFragmentByTag("fragmentCategoria");
        if (elFragment!=null && elFragment.isVisible() && elFragment2!=null){

            elfra.popBackStackImmediate("asd",FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        else if (elFragment2!=null&&elFragment2.isVisible() || elFragment2==null){
            ActivityVerPalabra.this.finish();

        }
        else{
            super.onBackPressed();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                    onBackPressed();
                break;
        }
        return true;
    }



    @Override
    public void abrirPalabra(String unString) {
        traerTextoPalabra(unString,"busqueda");
    }

    @Override
    public void abrirSugerencias() {
        SugerirFragment sugerirFragment = new SugerirFragment();
        android.support.v4.app.FragmentManager fragmentManager= getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerFragmentPalabra, sugerirFragment, "fragmentMostrarPalabra");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void abrirFuente(String unString) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(unString));
        startActivity(intent);
    }
}
