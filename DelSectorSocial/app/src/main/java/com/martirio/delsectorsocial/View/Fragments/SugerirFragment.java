package com.martirio.delsectorsocial.View.Fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.martirio.delsectorsocial.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SugerirFragment extends Fragment {


    public SugerirFragment() {
        // Required empty public constructor
    }

    private EditText editPalabra;
    private TextInputLayout textInputLayout;
    private ImageButton imageButton;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sugerir, container, false);

        editPalabra=view.findViewById(R.id.editPalabra);
        textInputLayout=view.findViewById(R.id.inputLayout);
        imageButton=view.findViewById(R.id.botonEnviarPalabra);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editPalabra.getText()==null || editPalabra.getText().toString().isEmpty()){
                    textInputLayout.setError(view.getContext().getResources().getString(R.string.ingreseTextoValido));
                }
                else {

                    try {

                        String recepientEmail = ""; // either set to destination email or leave empty
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse("mailto:" + "marcela@delsectorsocial.org"));
                        intent.putExtra(android.content.Intent.EXTRA_TEXT,
                                "Quiero sugerir la palabra: "+editPalabra.getText().toString()+"\n"+"Gracias!");
                        startActivity(intent);
                        editPalabra.setText("");


                    } catch (Exception e) {
                        Log.e("NOMAD/ERROR", "exception", e);
                    }

                }
            }
        });

        return view;
    }

    public static SugerirFragment crearSugerirFragment(){
        return  new SugerirFragment();
    }

}
