package com.martirio.delsectorsocial.View.Adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.martirio.delsectorsocial.View.Fragments.InicioFragment;
import com.martirio.delsectorsocial.View.Fragments.FragmentCategorias;
import com.martirio.delsectorsocial.View.Fragments.SugerirFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pablo on 31/5/2017.
 */

public class AdapterPagerFragment extends FragmentStatePagerAdapter {

    //EL ADAPTER NECESITA SIEMPRE UNA LISTA DE FRAGMENTS PARA MOSTRAR
    private List<Fragment> listaFragments;

    private List<String> unaListaTitulos;

    public AdapterPagerFragment(FragmentManager fm, List<String> listaTitulos) {
        super(fm);

        //INICIALIZO LA LISTA DE FRAGMENT
        unaListaTitulos=listaTitulos;
        listaFragments = new ArrayList<>();


        //LE CARGO LOS FRAGMENTS QUE QUIERO. UTILIZO LA LISTA DE PELICULAS Y SERIES PARA CREAR LOS FRAGMENTS.

        for (String unString : unaListaTitulos) {
            if (unString.equals("inicio")){
                listaFragments.add(InicioFragment.crearInicioFragment());
            }
            if (unString.equals("categorias")){
                listaFragments.add(FragmentCategorias.crearListadoFragment());
            }
            if (unString.equals("Sugerir Palabra")){
                listaFragments.add(SugerirFragment.crearSugerirFragment());
            }
        }

        //LE AVISO AL ADAPTER QUE CAMBIO SU LISTA DE FRAGMENTS.
        notifyDataSetChanged();
    }


    @Override
    public Fragment getItem(int position) {
        return listaFragments.get(position);
    }

    @Override
    public int getCount() {
        return listaFragments.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return unaListaTitulos.get(position);
    }

    public void setUnaListaTitulos(List<String> unaListaTitulos) {
        this.unaListaTitulos = unaListaTitulos;
    }

    public void limpiarAdapters(){
        InicioFragment inicioFragment=(InicioFragment) this.getItem(0);
        inicioFragment.limpiarAdapter();
        FragmentCategorias fragmentCategorias = (FragmentCategorias) this.getItem(1);
        fragmentCategorias.limpiarAdapter();
    }


}
