package com.martirio.delsectorsocial.View.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.martirio.delsectorsocial.Model.Page;
import com.martirio.delsectorsocial.Model.PaginaWiki;
import com.martirio.delsectorsocial.R;
import com.martirio.delsectorsocial.Utils.HTTPConnectionManager;
import com.martirio.delsectorsocial.Utils.ResultListener;
import com.martirio.delsectorsocial.View.Adapter.AdapterPalabrasListado;
import com.martirio.delsectorsocial.View.Controller.ControllerDatos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class InicioFragment extends Fragment {

    private TextView textoTitulo;
    private TextView subTitulo;
    private ImageButton botonBusqueda;


    private AutoCompleteTextView editBusqueda;
    private TextInputLayout textInputLayout;


    private RecyclerView recyclerListadoPalabras;
    private AdapterPalabrasListado adapterPalabrasListado;
    private String palabraBuscada;
    private ControllerDatos controllerDatosListado;

    private List<String> listaPalabras;
    private Avisable avisable;
    private ArrayList<String> unaLista;
    ArrayAdapter<String> adapterAutoComplete;

    private LinearLayout linearLayout;
    private ImageView lineaDivisoria;

    private SharedPreferences config;

    public void limpiarAdapter() {
        adapterPalabrasListado.setListaPalabrasOriginales(new ArrayList<String>());
        adapterPalabrasListado.notifyDataSetChanged();
        editBusqueda.setText("");
    }

    public interface Avisable{
        void clickearPalabra(String unString, String origen);
        void clickearCategoria(String unString, String origen);
        void limpiarAdapters();
    }




    public InicioFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inicio, container, false);
        textoTitulo=view.findViewById(R.id.subTituloMain);
        subTitulo=view.findViewById(R.id.tituloDescripcionInicial);
        botonBusqueda=view.findViewById(R.id.botonBuscar);
        editBusqueda=view.findViewById(R.id.editBusqueda);
        textInputLayout=view.findViewById(R.id.inputLayout);
        linearLayout=view.findViewById(R.id.linearImagenTitulo);
        lineaDivisoria=view.findViewById(R.id.lineaDivisoria);


        //RECYCLER Y ADAPTER - START

        controllerDatosListado=new ControllerDatos(getContext());
        recyclerListadoPalabras=view.findViewById(R.id.recyclerListadoPalabras);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerListadoPalabras.setLayoutManager(linearLayoutManager);
        adapterPalabrasListado=new AdapterPalabrasListado();
        adapterPalabrasListado.setContext(getContext());

        adapterPalabrasListado.setListaPalabrasOriginales(new ArrayList<String>());
        recyclerListadoPalabras.setAdapter(adapterPalabrasListado);
        adapterPalabrasListado.notifyDataSetChanged();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer posicion=recyclerListadoPalabras.getChildAdapterPosition(view);
                List<String> unaLista=adapterPalabrasListado.getListaPalabrasOriginales();
                String unString=unaLista.get(posicion);
                if (HTTPConnectionManager.isNetworkingOnline(getContext())) {
                    editBusqueda.setText("");
                    avisable.clickearPalabra(unString, "busqueda");
                }
                else{
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.sinConexion), Toast.LENGTH_SHORT).show();
                }
            }
        };

        adapterPalabrasListado.setListener(listener);

        //RECYCLER Y ADAPTER - END



        Typeface robotoR = Typeface.createFromAsset(view.getContext().getAssets(), "fonts/Roboto-Regular.ttf");

        textoTitulo.setTypeface(robotoR);
        subTitulo.setTypeface(robotoR);

       botonBusqueda.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if (editBusqueda.getText()==null || editBusqueda.getText().toString().isEmpty()) {

                   textInputLayout.setError("Ingrese palabra a buscar");
               } else {
                   if (HTTPConnectionManager.isNetworkingOnline(getContext())) {
                       avisable.clickearCategoria(editBusqueda.getText().toString(),"buscada");
                   }
                   else{
                       Toast.makeText(getContext(), getContext().getResources().getString(R.string.sinConexion), Toast.LENGTH_SHORT).show();
                   }
               }
           }
       });

       //TEXT CAHNGE WATCHER - START
        editBusqueda.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                linearLayout.setVisibility(View.GONE);
                lineaDivisoria.setVisibility(View.GONE);
                if (config!=null){

                    final String categoriaElegida =config.getString("categoriaElegida","Argentina");
                        actualizarLista(s.toString(),categoriaElegida);
                }
            }
        });
        //TEXT CAHNGE WATCHER - END


        //AUTOCOMPLETE TEXTVIEW - START
/*
        adapterAutoComplete = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,new ArrayList<String>());
        editBusqueda.setAdapter(adapterAutoComplete);
*/

        //SI ES LA PRIMERA VEZ QUE ENTRO A LA APP CORRE EL MENSAJE DE LA CATEGORIA
        config = getActivity().getSharedPreferences("prefs",0);
        final boolean primeraVez = config.getBoolean("esPrimeraVez",true);
        final String categoriaElegida =config.getString("categoriaElegida","Argentina");
        if (primeraVez){

            List<String> unaLista=new ArrayList<>();
            unaLista.add("Argentina");
            unaLista.add("Colombia");
            unaLista.add("España");
            unaLista.add("Francia");

            MaterialDialog dialog = new MaterialDialog.Builder(getContext())
                    .title("Bienvenid@")
                    .items(unaLista)
                    .content(getContext().getResources().getString(R.string.elijaPais)+"\n"+getContext().getResources().getString(R.string.elijaPais2))
                    .widgetColor(getContext().getResources().getColor(R.color.tile3))
                    .cancelable(false)
                    .positiveText(R.string.aceptar)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                        @Override
                        public boolean onSelection(MaterialDialog dialog, View view, final int which, final CharSequence text) {
                            SharedPreferences.Editor editor = config.edit();
                            editor.putString("categoriaElegida",calcularPais(text));
                            editor.putBoolean("esPrimeraVez",false);
                            editor.commit();
                            avisable.limpiarAdapters();

                            return true;
                        }
                    })





                    .build();
            dialog.show();





        }

        //AUTOCOMPLETE TEXTVIEW - END
        return view;
    }

    private String calcularPais(CharSequence text) {
        String recibido=text.toString();
        String procesado;
        switch (recibido){
            case "Francia":
                procesado = "France";
                break;

            default:
                procesado= recibido;
                break;
        }
        return  procesado;
    }


    private void actualizarLista(final String palabraBuscada) {

        if (HTTPConnectionManager.isNetworkingOnline(getContext())) {
            controllerDatosListado.traerTitulosConPalabra(new ResultListener<PaginaWiki>() {

                @Override
                public void finish(PaginaWiki paginaWiki) {

                    if (paginaWiki!=null && paginaWiki.getData()!=null && paginaWiki.getData().getPagesCat()!=null) {
                        listaPalabras=new ArrayList<>();
                        for (Page unaPage:paginaWiki.getData().getPagesCat()
                                ) {

                            listaPalabras.add(unaPage.getTitle());

                        }
                        actualizarRecycler(listaPalabras);
                    }

                }


            },palabraBuscada);
        }
        else {
           Snackbar elSnack = Snackbar.make(getView(),getContext().getString(R.string.sinConexion),Snackbar.LENGTH_LONG)
                    .setAction("Reintentar?", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            actualizarLista(palabraBuscada);
                        }
                    });
           elSnack.show();

        }
    }

    private void actualizarLista(final String palabraBuscada, String categoria) {

        if (HTTPConnectionManager.isNetworkingOnline(getContext())) {
            controllerDatosListado.traerTitulosConPalabra(new ResultListener<PaginaWiki>() {

                @Override
                public void finish(PaginaWiki paginaWiki) {

                    if (paginaWiki!=null && paginaWiki.getData()!=null && paginaWiki.getData().getPagesCat()!=null) {
                        listaPalabras=new ArrayList<>();
                        for (Page unaPage:paginaWiki.getData().getPagesCat()
                                ) {

                            listaPalabras.add(unaPage.getTitle());

                        }
                        actualizarRecycler(listaPalabras);
                    }

                }


            },palabraBuscada,categoria);
        }
        else {
            Snackbar elSnack = Snackbar.make(getView(),getContext().getString(R.string.sinConexion),Snackbar.LENGTH_LONG)
                    .setAction("Reintentar?", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            actualizarLista(palabraBuscada);
                        }
                    });
            elSnack.show();

        }
    }


    private void actualizarRecycler(List<String> listaPalabras) {
        adapterPalabrasListado.setListaPalabrasOriginales(listaPalabras);
        adapterPalabrasListado.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.avisable=(Avisable) context;
    }

    public static InicioFragment crearInicioFragment(){
        return  new InicioFragment();
    }

}
