package com.martirio.delsectorsocial.Utils;

public class DAOException extends Exception {

	public DAOException(String error)
	{
		super(error);
	}
	
}
